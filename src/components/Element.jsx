import React from 'react';
import PropTypes from 'prop-types';

import Card from './Card';
import Link from './Link';
import { getHost } from '../services/url';

const Element = ({ id, title, url, descendants, by, score, showStory }) => (
  <div>Empty Element</div>
);

Element.propTypes = {
};

Element.defaultProps = {
};

export default Element;
