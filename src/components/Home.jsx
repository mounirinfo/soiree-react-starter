import React from 'react';
import PropTypes from 'prop-types';

import Cell from './Cell';
import Columns from './Columns';
import Element from './Element';
import Button from './Button';
import Loader from './Loader';

const Home = ({ elements, error, loading, loadMore, showStory }) => (
  <div>Empty Home</div>
);

Home.propTypes = {
};

export default Home;
